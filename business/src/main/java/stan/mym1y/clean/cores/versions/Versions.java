package stan.mym1y.clean.cores.versions;

public interface Versions
{
    long version();
    long currencies();
}