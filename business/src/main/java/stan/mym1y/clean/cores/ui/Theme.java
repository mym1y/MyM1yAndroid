package stan.mym1y.clean.cores.ui;

public interface Theme
{
    boolean isDarkTheme();
    Colors colors();
}