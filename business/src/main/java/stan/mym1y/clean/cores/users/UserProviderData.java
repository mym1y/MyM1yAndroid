package stan.mym1y.clean.cores.users;

public interface UserProviderData
{
    String tokenId();
}