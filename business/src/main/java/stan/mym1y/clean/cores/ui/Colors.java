package stan.mym1y.clean.cores.ui;

public interface Colors
{
    int background();
    int foreground();
    int accent();
    int positive();
    int negative();
    int neutral();
    int alert();
    int confirm();
}