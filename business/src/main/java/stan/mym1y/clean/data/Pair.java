package stan.mym1y.clean.data;

public interface Pair<F, S>
{
    F first();
    S second();
}