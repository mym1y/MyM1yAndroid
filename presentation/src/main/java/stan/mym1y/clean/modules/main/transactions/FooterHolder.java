package stan.mym1y.clean.modules.main.transactions;

import android.content.Context;
import android.view.ViewGroup;

import stan.mym1y.clean.R;
import stan.mym1y.clean.units.adapters.Holder;

class FooterHolder
    extends Holder
{
    FooterHolder(Context context, ViewGroup parent)
    {
        super(context, parent, R.layout.empty_footer);
    }
}