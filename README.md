![compileSdkVersion 26](https://img.shields.io/badge/compileSdkVersion-26-yellow.svg) ![buildToolsVersion 26.0.2](https://img.shields.io/badge/buildToolsVersion-26.0.2-blue.svg) ![minSdkVersion 15](https://img.shields.io/badge/minSdkVersion-15-red.svg) ![targetSdkVersion 25](https://img.shields.io/badge/targetSdkVersion-25-green.svg)

<h1 align="center">MyM1yAndroid</h1>

<div align="center">
  <img src="media/icon.png"/>
</div>
<div align="center">
  <strong>MyM1y Android client app</strong>
</div>

## :wrench: Architecture

- [MVP](https://gitlab.com/mym1y/MyM1yAndroid) architecture
- [DAO](https://gitlab.com/mym1y/MyM1yAndroid) layer
- flexible [Dependency Injection](https://gitlab.com/mym1y/MyM1yAndroid) system
- painless jump from [SQLite](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html) to [NoSQL](https://gitlab.com/mym1y/MyM1yAndroid) Object-Relational Mapping [Boxes](https://github.com/StanleyProjects/Boxes)
- also painless jump from [SharedPreferences](https://developer.android.com/reference/android/content/SharedPreferences.html) to Cases feature of [Boxes](https://github.com/StanleyProjects/Boxes) lib

## :neutral_face: Authentication

- used the [Google Identity Toolkit](https://developers.google.com/identity/toolkit) and [Firebase Realtime Database](https://firebase.google.com/docs/database)
- also used the [Google oauth2 api](https://www.googleapis.com/oauth2/v4/token) for authorized with [Google account](https://accounts.google.com)

## :art: Design

- used the custom [Navigation Drawer](https://gitlab.com/mym1y/MyM1yAndroid)
- used own platform independent tool for animations - [ValueAnimator](https://gitlab.com/mym1y/MyM1yAndroid)