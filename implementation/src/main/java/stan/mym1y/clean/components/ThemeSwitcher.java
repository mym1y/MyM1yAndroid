package stan.mym1y.clean.components;

import stan.mym1y.clean.cores.ui.Theme;

public interface ThemeSwitcher
{
    void switchTheme(Theme theme);
    Theme theme();
}