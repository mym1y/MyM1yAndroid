package stan.mym1y.clean.boxes;

import java.util.HashMap;
import java.util.Map;

import stan.boxes.Case;
import stan.boxes.ORM;
import stan.mym1y.clean.boxes.access.CashAccounts;
import stan.mym1y.clean.boxes.access.Currencies;
import stan.mym1y.clean.boxes.access.Transactions;
import stan.mym1y.clean.data.local.DAO;
import stan.mym1y.clean.data.local.access.CashAccountsAccess;
import stan.mym1y.clean.data.local.access.CurrenciesAccess;
import stan.mym1y.clean.data.local.access.TransactionsAccess;

public class Boxes
    implements DAO
{
    static private final long VERSION = 1710160953;

    private final Case<Long> versionCase;
    private final CurrenciesAccess currenciesAccess;
    private final TransactionsAccess transactionsAccess;
    private final CashAccountsAccess cashAccountsAccess;

    public Boxes(String path)
    {
        versionCase = new Case<>(-1L, new ORM<Long>()
        {
            private final String VERSION = "version";

            public Map write(Long value)
            {
                Map map = new HashMap();
                map.put(VERSION, value);
                return map;
            }
            public Long read(Map map)
            {
                return (Long)map.get(VERSION);
            }
        }, path + "/version");
        currenciesAccess = new Currencies(path);
        transactionsAccess = new Transactions(path);
        cashAccountsAccess = new CashAccounts(path);
    }
    private void checkVersion()
    {
        long v = versionCase.get();
        if(v != VERSION)
        {
            upgrade(v, VERSION);
            versionCase.save(VERSION);
        }
    }

    public CurrenciesAccess currenciesAccess()
    {
        return currenciesAccess;
    }
    public TransactionsAccess transactionsAccess()
    {
        return transactionsAccess;
    }
    public CashAccountsAccess cashAccountsAccess()
    {
        return cashAccountsAccess;
    }

    private void upgrade(long oldVersion, long newVersion)
    {
        currenciesAccess.currencies().clear();
        transactionsAccess.transactions().clear();
        cashAccountsAccess.cashAccounts().clear();
    }
}