package stan.mym1y.clean.utils;

public interface Connector<T>
{
    void connect(T t);
}