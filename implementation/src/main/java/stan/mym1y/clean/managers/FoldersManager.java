package stan.mym1y.clean.managers;

import android.util.Log;

import java.io.File;

import stan.mym1y.clean.components.FoldersAccess;

public class FoldersManager
    implements FoldersAccess
{
    private String filesDirectory;

    public FoldersManager(String fd)
    {
        filesDirectory = fd;
        checkRoot();
        log("Root - " + filesDirectory);
    }
    private void checkRoot()
    {
        check(getDataBasePath(), "DataBase");
    }
    private void check(String path, String type)
    {
        File dir = new File(path);
        dir.mkdirs();
        if(dir.exists())
        {
            log(type + " Directory - " + path);
        }
        else
        {
            String message = "create " + type + " directory failed!!!";
            logE(message);
            throw new RuntimeException(message);
        }
    }

    public String getDataBasePath()
    {
        return filesDirectory + "/database";
    }

    private void log(String message)
    {
        Log.i("["+getClass().getSimpleName()+"]", message);
    }
    private void logE(String message)
    {
        Log.e("["+getClass().getSimpleName()+"]", message);
    }
}